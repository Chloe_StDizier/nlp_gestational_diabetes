import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import requests
import re 


# Import des posts nettoyés

df = pd.read_csv("../data/cleaned_posts.csv", encoding = 'utf-8', sep = ';', decimal = ',')



# Ajout des colonnes metformin et insulin (1 si le post contient le nom, 0 sinon)

df["metformin"] = df["text"].str.contains("metformin") 

df["insulin"] = df["text"].str.contains("insulin")


# Ajout d'un colone drug définissant les médicaments metionnés dans le post : "Metfomin and insulin", "Only metformin", "Only insulin"

d = []


for i in range(df.shape[0]) :

	if (df["metformin"][i] & df["insulin"][i]) : 
		d.append("Metformin and insulin")

	elif df["metformin"][i] :
		d.append("Only metformin")

	else :
		d.append("Only insulin")


df["drug"] = d



############################ VISUALISATION ######################################

# Nombre de posts par médicaments

posts_per_drug = df['drug'].value_counts()
drugs = posts_per_drug.index.tolist()
posts = posts_per_drug.tolist()

plt.bar(drugs, posts, color='lightblue')
plt.ylabel('Posts\n')
plt.title('Number of posts per drug')
# plt.savefig("../images/posts_per_drug.png")
plt.show()



# Nombre moyen de mots par post par médicament

df['word_count'] = df['text'].str.split().apply(len)

drug_word_count = df.groupby('drug')['word_count'].mean()


plt.bar(drug_word_count.index, drug_word_count.tolist(), color='lightblue')
plt.ylabel('Average Word Count per Post')
plt.title('Word count per post per drug')
# plt.savefig("../images/mean_word_count_per_drug.png")
plt.show()




# Nuages de mots

## diabète gestationnel : pas pertinent car groupe centré sur ce sujet
## insulin et metformin pareil car critère d'inclusion 
## week : pas pertinents car toutes les femmes décrivent le contexte en indiquant le nombre de semaine de grossesses

mots_a_exclure = ['metformin', 'gestational', 'diabetes', 'insulin', 'week']


	# wordcloud tous posts confondus  
wordcloud = WordCloud(background_color ='aliceblue',min_font_size = 10, stopwords = mots_a_exclure, colormap = "Dark2").generate(' '.join(df["text"]))

plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.title("All posts")
# plt.savefig("../images/worcloud_all.png")
plt.show()



	# wordcloud "Only metformin" posts 
wordcloud = WordCloud(background_color ='aliceblue',
	min_font_size = 10, 
	stopwords = mots_a_exclure, 
	colormap = "Dark2").generate(' '.join(df[df.drug == "Only metformin"]["text"]))
 
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.title("Only metformin")
# plt.savefig("../imagesworcloud_metfo.png")
plt.show()


	# wordcloud "Only insulin" posts 
wordcloud = WordCloud(background_color ='aliceblue',
	min_font_size = 10, 
	stopwords = mots_a_exclure, 
	colormap = "Dark2").generate(' '.join(df[df.drug == "Only insulin"]["text"]))
 
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.title("Only insulin")
# plt.savefig("../images/worcloud_insu.png")
plt.show()



	# wordcloud "Metformin and insulin" posts 
wordcloud = WordCloud(background_color ='aliceblue',
	min_font_size = 10, 
	stopwords = mots_a_exclure, 
	colormap = "Dark2").generate(' '.join(df[df.drug == "Metformin and insulin"]["text"]))
 
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.title("Metformin and insulin")
# plt.savefig("..images/worcloud_both.png")
plt.show()




############################ ANALYSES ######################################



# Import du modèle de NER depuis hugging face

API_URL = "https://api-inference.huggingface.co/models/d4data/biomedical-ner-all"
headers = {"Authorization": "Bearer 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"}


# Fonction pour appliquer le modèle 

def query(payload):
	response = requests.post(API_URL, headers=headers, json=payload)
	return response.json()


# Application du modèle sur les posts "Metformin and insulin"

output = query({
	"inputs": df[df.drug == "Metformin and insulin"]['text'].tolist(),
})


# Récupération des mots reconnus comme signes et symptômes : entity_group = "Sign_symptom"

	## initialisation

sign_both = []
related_post_both = []

post_nb = 0

for l in output :
	# parcours de chaque post 

	nb_sign = 0

	for d in l :
		# parcours de chaque mot taggué


		# récupération uniquement des signes et symptômes et du post associé (identifié par post_nb )

		try :
			if d['entity_group'] == "Sign_symptom" :
				sign_both.append(d['word'])
				nb_sign = nb_sign + 1

				related_post_both.append(post_nb)


		except :
			nb_sign = nb_sign 

			if nb_sign == 0 :
				sign_both.append("no_sign")
				related_post_botht.append(post_nb)		

	post_nb = post_nb + 1

	

	## stockage dans un data frame 

df_both = pd.DataFrame({'sign' : sign_both, 'post_number' : related_post_both})





# Application du modèle sur les posts "Only metformin"


output = query({
	"inputs": df[df.drug == "Only metformin"]['text'].tolist(),
})


# Récupération des mots reconnus comme signes et symptômes : entity_group = "Sign_symptom"

	## initialisation

sign_metfo = []
related_post_metfo = []

post_nb = 0

for l in output :

	nb_sign = 0

	for d in l :


		try :
			if d['entity_group'] == "Sign_symptom" :
				sign_metfo.append(d['word'])
				nb_sign = nb_sign + 1

				related_post_metfo.append(post_nb)


		except :
			nb_sign = nb_sign 

			if nb_sign == 0 :
				sign_metfo.append("no_sign")
				related_post_metfo.append(post_nb)		



	post_nb = post_nb + 1


	## stockage dans un data frame 

df_metfo = pd.DataFrame({'sign' : sign_metfo, 'post_number' : related_post_metfo})




# Application du modèle sur la première partie des posts "Only insulin" (impossible d'éxecuter sur un trop grand nombre de posts en une fois)


output = query({
	"inputs": df[df.drug == "Only insulin"]['text'].tolist()[0:df[df.drug == "Only insulin"].shape[0]//2],
})


# Récupération des mots reconnus comme signes et symptômes : entity_group = "Sign_symptom"

	## initialisation

sign_insu = []
related_post_insu = []

post_nb = 0

for l in output :

	nb_sign = 0

	for d in l :


		try :
			if d['entity_group'] == "Sign_symptom" :
				sign_insu.append(d['word'])
				nb_sign = nb_sign + 1

				related_post_insu.append(post_nb)


		except :
			nb_sign = nb_sign 

			if nb_sign == 0 :
				sign_insu.append("no_sign")
				related_post_insu.append(post_nb)		



	post_nb = post_nb + 1


# Application du modèle sur le reste des posts "Only insulin" 

output = query({
	"inputs": df[df.drug == "Only insulin"]['text'].tolist()[df[df.drug == "Only insulin"].shape[0]//2:],
})


	## ajout à la suite des listes précédemment générées

for l in output :

	nb_sign = 0

	for d in l :

		
		try :
			if d['entity_group'] == "Sign_symptom" :
				sign_insu.append(d['word'])
				nb_sign = nb_sign + 1

				related_post_insu.append(post_nb)


		except :
			nb_sign = nb_sign 

			if nb_sign == 0 :
				sign_insu.append("no_sign")
				related_post_insu.append(post_nb)		



	post_nb = post_nb + 1


	## stockage dans un data frame

df_insu = pd.DataFrame({'sign' : sign_insu, 'post_number' : related_post_insu})





# Constitution de dictionnaire basée sur des regex pour 11 concepts d'intérêts


	## relatif à l'anxiété, le stres, la peur etc

anxiety = [r"[\a-z]*anx[\a-z]*", 
	r"[\a-z]*stress[\a-z]*", 
	r"[\a-z]*nervous[\a-z]*", 
	r"[\a-z]*scar[\a-z]*",
	r"[\a-z]*worr[\a-z]*", 
	r"[\a-z]*panic[\a-z]*", 
	r"[\a-z]*terrif[\a-z]*", 
	r"[\a-z]*freak[\a-z]*", 
	r"[\a-z]*fear[\a-z]*", 
	r"[\a-z]*parano[\a-z]*",
	r"[\a-z]*phobi[\a-z]*"]



	## relatif à la tristesse, la dépression, le désespoir etc

sadness = [r"[\a-z]*sad[\a-z]*", 
	r"[\a-z]*depres[\a-z]*",
	r"[\a-z]*upset[\a-z]*", 
	r"[\a-z]*cry[\a-z]*",
	r"[\a-z]*cri[\a-z]*", 
	r"[\a-z]*tear[\a-z]*", 
	r"[\a-z]*discourag[\a-z]*", 
	r"[\a-z]*mental breakdown[\a-z]*", 
	r"[\a-z]*lo[\a-z]s[\a-z]* hope[\a-z]*",
	r"[\a-z]*guilt[\a-z]*", 
	r"[\a-z]*defeat[\a-z]*", 
	r"[\a-z]*embarass[\a-z]*",
	r"[\a-z]*asham[\a-z]*"]



	## relatif à la fatigue, aux problèmes de sommeil etc

tired = [r"[\a-z]*tired[\a-z]*", 
	r"[\a-z]*sle[e]*p[\a-z]*", 
	r"[\a-z]*exhaust[\a-z]*", 
	r"[\a-z]*drain[\a-z]*", 
	r"[\a-z]*dizzy[\a-z]*", 
	r"[\a-z]*insomnia[\a-z]*", 
	r"[\a-z]*fatigue[\a-z]*"]


	## relatif à la faim, l'appétit etc

eat = [r"[\a-z]*crav[\a-z]*", 
	r"[\a-z]*starv[\a-z]*", 
	r"[\a-z]*hung[\a-z]*", 
	r"[\a-z]*eat[\a-z]*", 
	r"[\a-z]*food[\a-z]*", 
	r"[\a-z]*apetite[\a-z]*"]


	## relatif à la douleur

pain = [r"[\a-z]*pain[\a-z]*", 
	r"[\a-z]*hurt[\a-z]*", 
	r"[\a-z]*harm[\a-z]*", 
	r"[\a-z]*ache[\a-z]*"]


	## relatif au système digestif

digestive_issues = [r"[\a-z]*stomach[\a-z]*", 
	r"[\a-z]*diarrhea[\a-z]*", 
	r"[\a-z]*constipat[\a-z]*", 
	r"[\a-z]*pee[\a-z]*", 
	r"[\a-z]*heartburn[\a-z]*", 
	r"[\a-z]*stool[\a-z]*", 
	r"[\a-z]*tummy[\a-z]*"]


	## realtif à l'hypertension, la pression artérielle 

hypertension = [r"[\a-z]*tension[\a-z]*", r"press[\a-z]*"]


	## relatif aux saignements

bleeding = [r"[\a-z]*bleed[\a-z]*"]


	## relatif aux contractions

contraction = [r"[\a-z]*cramp[\a-z]*", r"[\a-z]*contraction[\a-z]*"]


	## relarif aux nausées, vomissements etc

nausea = [r"[\a-z]*nausea[\a-z]*", r"[\a-z]*thr[o,e]w[\a-z]*", r"[\a-z]*vomit[\a-z]*"]


	## realtif au froid

cold = [r"[\a-z]*cold[\a-z]*"]


# Fonction permettant d'indiquer si une liste de mots contient un syptôme d'une classe définie par un dictionnaire (renvoie 1 si oui, 0 si non)

def classify_sign(sign_list, dic) : 

	matching = [re.match('|'.join(dic), sign) for sign in sign_list]
	find = []

	for m in matching :
		if m != None :
			find.append(m)

	if len(find)  > 0 :
		return 1
	else : 
		return 0



# Identification des classes apparaissant ou non dans chaque post (pour chaque classe : ajout d'une variable binaire)

	## "Metformin and insulin"

df_both = df_both.groupby('post_number').agg(lambda x : list(x))

df_both['anxiety'] = [classify_sign(x, anxiety) for x in df_both['sign'].tolist()]
df_both['sadness'] = [classify_sign(x, sadness) for x in df_both['sign'].tolist()]
df_both['tired'] = [classify_sign(x, tired) for x in df_both['sign'].tolist()]
df_both['appetite'] = [classify_sign(x, eat) for x in df_both['sign'].tolist()]
df_both['pain'] = [classify_sign(x, pain) for x in df_both['sign'].tolist()]
df_both['digestion'] = [classify_sign(x, digestive_issues) for x in df_both['sign'].tolist()]
df_both['hypertension'] = [classify_sign(x, hypertension) for x in df_both['sign'].tolist()]
df_both['bleeding'] = [classify_sign(x, bleeding) for x in df_both['sign'].tolist()]
df_both['contraction'] = [classify_sign(x, contraction) for x in df_both['sign'].tolist()]
df_both['nausea'] = [classify_sign(x, nausea) for x in df_both['sign'].tolist()]
df_both['cold'] = [classify_sign(x, cold) for x in df_both['sign'].tolist()]


	## "Only metformin"

df_metfo = df_metfo.groupby('post_number').agg(lambda x : list(x))

df_metfo['anxiety'] = [classify_sign(x, anxiety) for x in df_metfo['sign'].tolist()]
df_metfo['sadness'] = [classify_sign(x, sadness) for x in df_metfo['sign'].tolist()]
df_metfo['tired'] = [classify_sign(x, tired) for x in df_metfo['sign'].tolist()]
df_metfo['appetite'] = [classify_sign(x, eat) for x in df_metfo['sign'].tolist()]
df_metfo['pain'] = [classify_sign(x, pain) for x in df_metfo['sign'].tolist()]
df_metfo['digestion'] = [classify_sign(x, digestive_issues) for x in df_metfo['sign'].tolist()]
df_metfo['hypertension'] = [classify_sign(x, hypertension) for x in df_metfo['sign'].tolist()]
df_metfo['bleeding'] = [classify_sign(x, bleeding) for x in df_metfo['sign'].tolist()]
df_metfo['contraction'] = [classify_sign(x, contraction) for x in df_metfo['sign'].tolist()]
df_metfo['nausea'] = [classify_sign(x, nausea) for x in df_metfo['sign'].tolist()]
df_metfo['cold'] = [classify_sign(x, cold) for x in df_metfo['sign'].tolist()]


	## "Only insulin"

df_insu = df_insu.groupby('post_number').agg(lambda x : list(x))

df_insu['anxiety'] = [classify_sign(x, anxiety) for x in df_insu['sign'].tolist()]
df_insu['sadness'] = [classify_sign(x, sadness) for x in df_insu['sign'].tolist()]
df_insu['tired'] = [classify_sign(x, tired) for x in df_insu['sign'].tolist()]
df_insu['appetite'] = [classify_sign(x, eat) for x in df_insu['sign'].tolist()]
df_insu['pain'] = [classify_sign(x, pain) for x in df_insu['sign'].tolist()]
df_insu['digestion'] = [classify_sign(x, digestive_issues) for x in df_insu['sign'].tolist()]
df_insu['hypertension'] = [classify_sign(x, hypertension) for x in df_insu['sign'].tolist()]
df_insu['bleeding'] = [classify_sign(x, bleeding) for x in df_insu['sign'].tolist()]
df_insu['contraction'] = [classify_sign(x, contraction) for x in df_insu['sign'].tolist()]
df_insu['nausea'] = [classify_sign(x, nausea) for x in df_insu['sign'].tolist()]
df_insu['cold'] = [classify_sign(x, cold) for x in df_insu['sign'].tolist()]



# Calcul du nombre de post mentionnant chaque classe de symptômes et affichage

print("\n\nMetformin et insulin (" + str(df[df.drug == "Metformin and insulin"].shape[0]) +" posts)")


print ("Anxiety : " + str(sum(df_both.anxiety)))
print ("Sadness : " + str(sum(df_both.sadness)))
print ("Tired : " + str(sum(df_both.tired)))
print ("Appetite : " + str(sum(df_both.appetite)))
print ("Pain : " + str(sum(df_both.pain)))
print ("Digestion : " + str(sum(df_both.digestion)))
print ("Hypertension : " + str(sum(df_both.hypertension)))
print ("Bleedin : " + str(sum(df_both.bleeding)))
print ("Contraction : " + str(sum(df_both.contraction)))
print ("Nausea : " + str(sum(df_both.nausea)))
print ("Cold : " + str(sum(df_both.cold)))



print("\n\nOnly metformin (" + str(df[df.drug == "Only metformin"].shape[0]) +" posts)")


print ("Anxiety : " + str(sum(df_metfo.anxiety)))
print ("Sadness : " + str(sum(df_metfo.sadness)))
print ("Tired : " + str(sum(df_metfo.tired)))
print ("Appetite : " + str(sum(df_metfo.appetite)))
print ("Pain : " + str(sum(df_metfo.pain)))
print ("Digestion : " + str(sum(df_metfo.digestion)))
print ("Hypertension : " + str(sum(df_metfo.hypertension)))
print ("Bleedin : " + str(sum(df_metfo.bleeding)))
print ("Contraction : " + str(sum(df_metfo.contraction)))
print ("Nausea : " + str(sum(df_metfo.nausea)))
print ("Cold : " + str(sum(df_metfo.cold)))


print("\n\nOnly insulin (" + str(df[df.drug == "Only insulin"].shape[0]) +" posts)")



print ("Anxiety : " + str(sum(df_insu.anxiety)))
print ("Sadness : " + str(sum(df_insu.sadness)))
print ("Tired : " + str(sum(df_insu.tired)))
print ("Appetite : " + str(sum(df_insu.appetite)))
print ("Pain : " + str(sum(df_insu.pain)))
print ("Digestion : " + str(sum(df_insu.digestion)))
print ("Hypertension : " + str(sum(df_insu.hypertension)))
print ("Bleedin : " + str(sum(df_insu.bleeding)))
print ("Contraction : " + str(sum(df_insu.contraction)))
print ("Nausea : " + str(sum(df_insu.nausea)))
print ("Cold : " + str(sum(df_insu.cold)))


print("\n\nAll posts (" + str(df.shape[0]) +" posts)")



print ("Anxiety : " + str(sum(df_insu.anxiety) + sum(df_both.anxiety) + sum(df_metfo.anxiety)))
print ("Sadness : " + str(sum(df_insu.sadness) + sum(df_both.sadness) + sum(df_metfo.sadness)))
print ("Tired : "+ str(sum(df_insu.tired) + sum(df_both.tired) + sum(df_metfo.tired)))
print ("Appetite : " + str(sum(df_insu.appetite) + sum(df_both.appetite) + sum(df_metfo.appetite)))
print ("Pain : " + str(sum(df_insu.pain) + sum(df_both.pain) + sum(df_metfo.pain)))
print ("Digestion : " + str(sum(df_insu.digestion) + sum(df_both.digestion) + sum(df_metfo.digestion)))
print ("Hypertension : " + str(sum(df_insu.hypertension) + sum(df_both.hypertension) + sum(df_metfo.hypertension)))
print ("Bleedin : " + str(sum(df_insu.bleeding) + sum(df_both.bleeding) + sum(df_metfo.bleeding)))
print ("Contraction : " + str(sum(df_insu.contraction) + sum(df_both.contraction) + sum(df_metfo.contraction)))
print ("Nausea : " + str(sum(df_insu.nausea) + sum(df_both.nausea) + sum(df_metfo.nausea)))
print ("Cold : " + str(sum(df_insu.cold) + sum(df_both.cold) + sum(df_metfo.cold)))






# Calcul du pourcentage de posts mentionnant les classes de symptômes

df_pourc = pd.DataFrame(
	{'sign_class' : df_both.columns[1:].tolist(),
	'both_posts' : [100 * sum(df_both[:][class_]) / df[df.drug == "Metformin and insulin"].shape[0] for class_ in df_both.columns[1:] ],
	'metfo_posts' : [100 * sum(df_metfo[:][class_]) / df[df.drug == "Only metformin"].shape[0] for class_ in df_metfo.columns[1:] ], 
	'insu_posts' : [100 * sum(df_insu[:][class_]) / df[df.drug == "Only insulin"].shape[0] for class_ in df_insu.columns[1:] ],
	'all_posts' : [100 * (sum(df_both[:][class_]) + sum(df_metfo[:][class_]) + sum(df_insu[:][class_])) / df.shape[0] for class_ in df_insu.columns[1:] ]
	}
).sort_values('all_posts', ascending = False)



# Visualisation
plt.bar(df_pourc.sign_class, df_pourc.all_posts, label='all', color ='lightblue')
plt.title('All posts')
plt.ylabel('%\n')
# plt.savefig("../images/all_posts_sign_class.png")
plt.show()

fig, axs = plt.subplots(3,1)
axs[0].bar(df_pourc.sign_class, df_pourc.insu_posts,  label='only inuslin', color ='lightblue')
axs[0].title.set_text('Only insulin')
axs[0].set(ylabel = '%\n')
axs[1].bar(df_pourc.sign_class, df_pourc.metfo_posts,  label='only inuslin', color ='lightblue')
axs[1].title.set_text('Only metformin')
axs[1].set(ylabel = '%\n')
axs[2].bar(df_pourc.sign_class, df_pourc.both_posts,  label='only inuslin', color ='lightblue')
axs[2].title.set_text('Metformin and insulin')
axs[2].set(ylabel = '%\n')
fig.tight_layout(pad = 0.5)
# plt.savefig("../images/per_drug_posts_sign_class.png")
plt.show()

