# -*- coding: utf-8 -*-

import requests
import bs4
import pandas as pd



# Base url
base_url ='https://community.whattoexpect.com/'  

url_topic = 'https://community.whattoexpect.com/forums/gestational-diabetes.html'


# Initialisation

nb_page = 530 # nombre de pages du groupe "Gestational diabetes" au 7 juin 2023

all_link = []

all_post = []


# Récupération du lien des posts sur chacune des pages du groupe "gestational diabtes"
for i in range(nb_page) :

	print(i)
	page = requests.get(url_topic, {'page' : i+1 })


	soup =  bs4.BeautifulSoup(page.content, 'html.parser')

	liste_posts = soup.find_all(class_ = "linkDiscussion")


	for post in liste_posts :

		all_link.append(post['href'])




# Export des liens
link_diab_gest = pd.DataFrame({ 
	"link" : all_link
})

link_diab_gest.to_csv("../data/link.csv", encoding = "utf-8", index = False, sep = ";", decimal = ",")



# Parcours de chaque page corespondant à un post et récupération du contenu (texte) du post
for link in all_link :

	print(link)

	page = requests.get(base_url + link)

	soup = bs4.BeautifulSoup(page.content, 'html.parser')

	try :
		post = soup.find(class_ = "__messageContent fr-element fr-view").text

	except :
		post = ""
	all_post.append(post)



# Export des posts
posts_diab_gest = pd.DataFrame({ 
	"text" : all_post
})


posts_diab_gest.to_csv("../data/gd_posts.csv", encoding = "utf-8", index = False, sep = ";", decimal = ",")




# Récupération des posts contenant "metformin"

p_filter = posts_diab_gest

	# passage en minuscule
p_filter.text = p_filter.text.str.lower()
	
	# retrait posts vides
p_filter = p_filter.dropna()
	
	#filtre sur metformin
p_filter = p_filter[p_filter['text'].str.contains("metformin")]

	#export
p_filter.to_csv("../data/metfo_gd_posts.csv", encoding = "utf-8", index = False, sep = ";", decimal = ",")




# Récupération des posts contenant "insulin"

p_filter2 = post_diab_gest

	# passage en minuscule
p_filter2.text = p_filter2.text.str.lower()

	# retrait posts vides
p_filter2 = p_filter2.dropna()

	#filtre sur insulin
p_filter2 = p_filter2[p_filter2['text'].str.contains("insulin")]

	# export
p_filter2.to_csv("../data/insu_gd_posts.csv", encoding = "utf-8", index = False, sep = ";", decimal = ",")



# Récupération des posts contenant auu moins l'un des mots "metformin" ou "insulin"

p_final = pd.concat([p_filter, p_filter2])
	
	# retrait des doublons
p_final_dedoublonnage = p_final.drop_duplicates(keep = 'first')

	#export
p_final_dedoublonnage.to_csv("../data/metfo_insu_gd_posts.csv", encoding = "utf-8", index = False, sep = ";", decimal = ",")

